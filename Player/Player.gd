extends KinematicBody2D

var motion: Vector2 = Vector2(0, 0)

const SPEED: int = 1500
const GRAVITY: float = 150.0
const UP: Vector2 = Vector2(0, -1)
const JUMP_SPEED: int = 3000
const WORLD_LIMIT: int = 4500
const BOSST_MULTIPLIER: float = 1.5


signal animate

func _physics_process(delta):
	applay_gravity()
	jump()
	move()
	animated()

	# if you remove the UP, player falls much faster after time
	move_and_slide(motion, UP)


func applay_gravity():
	if position.y > WORLD_LIMIT:
		get_tree().call_group("Gamestate", "end_game")

	if is_on_floor() and motion.y > 0:
		motion.y = 0
	elif is_on_ceiling():
		motion.y = 1
	else:
		motion.y += GRAVITY


func jump():
	if Input.is_action_pressed("jump") and is_on_floor():
		motion.y -= JUMP_SPEED
		$JumpSFX.play()


func move():
	if Input.is_action_pressed("left") and Input.is_action_pressed("right"):
		motion.x = 0
	elif Input.is_action_pressed("left"):
		motion.x = -SPEED
	elif Input.is_action_pressed("right"):
		motion.x = SPEED
	else: 
		motion.x = 0


func animated():
	emit_signal("animate", motion)


func hurt():
	get_of_floor_and_stop_for_one_frame()
	motion.y = -JUMP_SPEED

	$PainSFX.play()

	
func boost():
	get_of_floor_and_stop_for_one_frame()
	motion.y = -JUMP_SPEED * BOSST_MULTIPLIER


func get_of_floor_and_stop_for_one_frame():
	# get of the floor 
	position.y -= 1
	# stop physics for one frame 
	yield(get_tree(), "idle_frame")