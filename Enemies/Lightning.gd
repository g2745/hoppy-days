extends Node2D


const SPEED: int = 400


func _ready():
    # don't inherit position information from your parent 
    set_as_toplevel(true)
    global_position = get_parent().global_position


func _process(delta):
    position.y += SPEED * delta
    manage_collosion()


func manage_collosion():
    var collider = $Area2D.get_overlapping_bodies()
    for object in collider:
        if object.name == "Player":
            get_tree().call_group("Gamestate", "hurt")
        queue_free()


func _on_VisibilityNotifier2D_screen_exited():
    queue_free()
