extends Node2D

var timeout: bool = false


func _process(delta):
    if $Sprite/RayCast2D.is_colliding():
        fire()


func fire():
    # load instance from this scene, equals a node
    var lightningNode: Node = load("res://Enemies/Lightning.tscn").instance()

    if not timeout:
        $Sprite/RayCast2D.add_child(lightningNode)
        $Sprite/Timer.start()
        timeout = true


func _on_Timer_timeout():
    timeout = false
