extends Node2D

var taken: bool = false

func _on_Area2D_body_entered(body:Node):
	if not taken:
		taken = true
		$AnimationPlayer.play("die")
		$CoinSFX2D.play()
		get_tree().call_group("Gamestate", "coin_up")

func die():
	queue_free()
