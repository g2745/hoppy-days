extends Area2D


func _on_SpikeTop_body_entered(body: Node):
    get_tree().call_group("Gamestate", "hurt")


func _on_SpikeBottom_body_entered(body: Node):
    get_tree().call_group("Gamestate", "hurt")
