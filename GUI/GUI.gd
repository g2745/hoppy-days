extends CanvasLayer


func _ready():
	$Control/TextureRect/HBoxContainer/LifeCount.text = "3"


func update_lives(lives_left: int):
	$Control/TextureRect/HBoxContainer/LifeCount.text = str(lives_left)


func update_coins(coins: int):
	$Control/TextureRect/HBoxContainer/CointCount.text = str(coins)
