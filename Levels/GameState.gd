extends Node2D

var lives: int = 3
var coins: int = 0
var coins_for_new_live: int = 10


func _ready():
	add_to_group("Gamestate")
	update_GUI_lives()


func hurt():
	lives -= 1
	$Player.hurt()
	update_GUI_lives()

	if lives < 1:
		end_game()


func coin_up():
	coins += 1
	update_GUI_coins()

	var enough_coins = (coins % coins_for_new_live) == 0
	if enough_coins:
		life_up()


func life_up():
	lives += 1
	update_GUI_lives()


func update_GUI_lives():
	get_tree().call_group("GUI", "update_lives", lives)


func update_GUI_coins():
	get_tree().call_group("GUI", "update_coins", coins)


func end_game():
	get_tree().change_scene("res://Levels/GameOver.tscn");


func win_game():
	get_tree().change_scene("res://Levels/Victory.tscn");
